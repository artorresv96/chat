import { Component, OnInit, Renderer2, LOCALE_ID, Inject, ElementRef } from '@angular/core';
import { FirestoreService } from './services/firestore/firestore.service';
import { oc } from 'ts-optchain';
import { environment } from '../environments/environment';
import { Survey } from './survey';
import { GenesysService } from './services/genesys/genesys.service';
import * as $ from "jquery"
import { Stats } from './stats';
import { Banner } from './banner';
import { Schedule } from './schedule';
import { getCurrentDay, getCurrentDateTime, getTimeStamp, getCurrentTime } from './helpers/calendar';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import widgetLanguage from '../locale/widgets.language.json';
import { ScriptTarget } from 'typescript';
// import { TranslateService } from '@ngx-translate/core';
import { Data } from './Data.model';


interface ChatMessage {
  data?: {
    data?: {
      name?: string,
      type?: string
      originalMessage?: {
        text?: string
      }
    }
  };
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Chat Field Support';
  updateTooltip = this.getTranslation('UpdateTooltip');
  stats: Stats;
  banner: Banner[];
  schedule: Schedule;
  bannerMessage: string;
  isChatAvailable = true;
  texto : string = 'chat modificado';

  constructor(
    @Inject(LOCALE_ID) protected localeId: string,
    private renderer: Renderer2,
    private elementRef: ElementRef,
    private data: Data
    // private translate: TranslateService
    // private firestoreService: FirestoreService,
    // private genesysService: GenesysService,
  ) { 
    // translate.setDefaultLang('es');
    // translate.use(localeId);    
    // this.data.translate = translate;
  }
  // form = {
  //   wrapper: '<table></table>',
  //   inputs: [
  //     {
  //       name: 'BP',
  //       placeholder: '@i18n:webchat.ChatFormBPPlaceholder',
  //       label: 'BP',
  //       // Check BP field
  //       validate: (event, form, input): boolean => {
  //         if (input) {
  //           if (input[0].value !== '') {
  //             return true;
  //           }
  //         }
  //         return false;
  //       }
  //     },
  //     {
  //       name: 'nickname',
  //       placeholder: '@i18n:webchat.ChatFormPlaceholderNickname',
  //       label: '@i18n:webchat.ChatFormNickname'
  //     },
  //   ]
  // };

  ngOnInit() {
    $('#chatStatus').text(this.texto);
    console.log(this.data)
    

    this.mainAsync().then(() => {
      // Load Genesys Widgets
    // const script = this.renderer.createElement('script');
    // script.src = 'assets/js/widgets.min.js';
    // //script.src = 'https://static.zdassets.com/ekr/snippet.js?key=ceaed84a-e17a-4f47-b6dc-4d31fca9d52e'
      
    // script.onload = () =>  {
    //      console.log(script)
    //  }
    // this.renderer.appendChild(document.head, script);
      
    //   window.zESettings = {
    //     helpCenter: {
    //         suppress: true
    //     },
    //     contactForm: {
    //         suppress: true
    //     },
    //     chat: {
    //         suppress: false
    //     },
    //     talk: {
    //         suppress: true
    //     }
    // }
      
      // Set Endpoint
      // const endpoint: string = this.localeId === 'es' ? 'https://ltm-gms.LTM.e-contact.cl/genesys/2/chat/FS' : this.localeId === 'pt' ? 'https://ltm-gms.LTM.e-contact.cl/genesys/2/chat/FS_BR' : '';
      // window.zESettings = {
        
      // }
      // if (!window._genesys) { window._genesys = {}; }
      // window._genesys.widgets = {
      //   main: {
      //     theme: 'light',
      //     lang: this.localeId,
      //     debug: false,
      //     i18n: widgetLanguage
      //   },
      //   webchat: {
      //     apikey: '',
      //     dataURL: endpoint,
      //     userData: {},
      //     emojis: true,
      //     uploadsEnabled: true,
      //     confirmFormCloseEnabled: true,
      //     actionsMenu: true,

      //     chatButton: {
      //       enabled: this.isChatAvailable,
      //       template: false,
      //       effect: 'fade',
      //       openDelay: 300,
      //       effectDuration: 150,
      //       hideDuringInvite: true
      //     },
      //     form: this.form,
      //     ajaxTimeout: 15000
      //   }
      // };


      // Genesys Extension
      // if (!window._genesys.widgets.extensions) { window._genesys.widgets.extensions = {}; }

      // window._genesys.widgets.extensions.Extension = ($, CXBus, Common): void => {
      //   const oExtension = CXBus.registerPlugin('Extension');
      //   const messages: any[] = [];
      //   // Check if a session exists
      //   if (!document.cookie.match(/^(.*;)?\s*_genesys.widgets.webchat.state.session\s*=\s*[^;]+(.*)?$/) && this.isChatAvailable) {
      //     oExtension.command('WebChat.open');
      //   }
      //   oExtension.subscribe('WebChat.messageAdded', (e) => {
      //     const chatMessage: ChatMessage = e;
      //     const name = oc(chatMessage).data.data.name();
      //     const message = oc(chatMessage).data.data.originalMessage.text();
      //     const type = oc(chatMessage).data.data.type();
      //     const time = getCurrentTime();
      //     if (message !== undefined) { messages.push({ message, name, type, time }); }
      //   });
      //   oExtension.subscribe('WebChat.completed', (e) => {
      //     messages.forEach((o, i) => o.id = i + 1);
      //     const survey = new Survey();
      //     survey.clientBP = e.data.form.BP;
      //     survey.client = e.data.form.nickname;
      //     survey.conversation = messages;
      //     this.getSessionData(oExtension, survey).then((survey) => {
      //       this.firestoreService.saveSurvey(survey).then(doc => {
      //         oExtension.command('WebChat.injectMessage', {
      //           type: 'html',
      //           name: '',
      //           text: `
      //           ${this.getTranslation('SurveyInvitation')}<br>
      //           <div class="like grow"><i class="lni-thumbs-up size-sm"></i></div>
      //           <div class="dislike grow"><i class="lni-thumbs-down size-sm"></i></div>
      //           <br>
      //           <form id="survey" style="display: none">
      //           <textarea
      //           class="survey-textarea"
      //           maxlength="64"
      //           placeholder="${this.getTranslation('SurveyPlaceHolder')}"
      //           name="comment"></textarea>
      //           <br>
      //           <input
      //           name="submit"
      //           class="cx-btn cx-btn-primary"
      //           type="submit"
      //           value="${this.getTranslation('SurveySendButton')}">
      //           </form>
      //           <div class="spacer"></div>
      //           `,
      //           custom: true,
      //           bubble: {
      //             fill: '#f2f4f7',
      //             radius: '4px',
      //             time: true,
      //             name: false,
      //             direction: 'right',
      //           }
      //         }).done(() => {
      //           document.querySelectorAll('.like, .dislike')
      //             .forEach((div): void => {
      //               div.addEventListener('click', (): void => {
      //                 const surveyForm = document.getElementById('survey');
      //                 surveyForm.style.display = 'inline';
      //                 surveyForm.scrollIntoView(false);
      //                 const active = document.querySelector('.active');
      //                 if (active != null) {
      //                   active.classList.remove('active');
      //                 }
      //                 div.classList.add('active');
      //               });
      //             });
      //           document.querySelector('#survey').addEventListener('submit', (event): void => {
      //             event.preventDefault();
      //             const survey = new Survey();
      //             const thumb = document.querySelector('.active');
      //             // Set Survey Vote
      //             survey.vote = thumb.classList.contains('like') ? 1 : thumb.classList.contains('dislike') ? 0 : null;
      //             // Set Survey Comment
      //             // tslint:disable-next-line:no-string-literal
      //             survey.comment = event.target['comment'].value || null;
      //             // Update the survey
      //             this.firestoreService.updateSurvey(doc.id, survey);
      //             // tslint:disable-next-line:no-string-literal
      //             const input = event.target['submit'];
      //             input.disabled = true;
      //             input.classList.remove('cx-btn-primary');
      //           });
      //         });
      //       });
      //     });

      //   });
      // };

    })
  }

  async mainAsync() {
    // Load the banner object from firestore
   // this.banner = await this.firestoreService.getBannerMessage();
    // Load the message item
    // this.bannerMessage = this.banner
    //   .find(element => element.language === this.localeId && element.message !== '')
    //   ?.message
      // console.log(this.localeId)
    // Display message if exists
    // if (this.bannerMessage !== undefined) {
    //   this.displayErrorAlert(this.bannerMessage);
    // }
    // Check if chat is enabled from the banner object
    // const isChatEnabled = this.banner.find(element => element.language === this.localeId)?.isChatAvailable;
    // Load the schedule object from firestore
    // this.schedule = await this.firestoreService.getSchedule();
    // Check if is opening hours
    // const isChatBusinessHours = this.checkSchedule(this.schedule);
    // Display message if out of business hours and there is no banner message
    // if (!isChatBusinessHours && this.bannerMessage === undefined) {
    //   this.displayErrorAlert(this.getTranslation('OutOfBusinessHours'))
    // }
    // Set bool variable if the chat is enable and within business hours
    // this.isChatAvailable = isChatEnabled && isChatBusinessHours;
    // Display Queue Stats
    // this.getStats();
  }

  displayErrorAlert(message) {
    Swal.fire({
      icon: 'warning',
      title: 'Oops...',
      text: message,
    })
  }

  // checkSchedule(schedule: Schedule): boolean {
  //   const date = new Date();
  //   const hour = date.getHours();
  //   const today = date.getDay();
  //   const currentDay = getCurrentDay();

  //   const isBusinessHour = hour >= schedule.startingHour && hour < schedule.endingHour;
  //   const isBusinessDay = today >= schedule.startingDay && today <= schedule.endingDay;

  //   let isHoliday: boolean;

  //   if (this.localeId === 'es') {
  //     isHoliday = schedule.blackoutDatesSSC.includes(currentDay);
  //   }
  //   if (this.localeId === 'pt') {
  //     isHoliday = schedule.blackoutDatesBrasil.includes(currentDay)
  //   }
  //   return isBusinessHour && isBusinessDay && !isHoliday
  // }





  getTranslation(value: string): string {
    for (const key in widgetLanguage) {
      if (key === this.localeId) {
        return widgetLanguage[key].webchat[value];
      }
    }
  }

  getStats() {
    // this.genesysService.getChatStats()
    //   .subscribe((res: Stats) => {
    //     this.stats = res;
    //   }, (error) => {
    //     console.error('error caught in component')
    //   });
  }

  language(isoLanguage){
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.textContent = "zE.setLocale('".concat(isoLanguage).concat("')");
    this.elementRef.nativeElement.appendChild(s);
    console.log(isoLanguage);
    this.localeId = isoLanguage;
    // getTranslation()
  }

  getSessionData(oExtension, survey): Promise<Survey> {
    return new Promise((resolve) => {
      oExtension.command('WebChatService.getStats').done((stats): void => {
        survey.datetime = getCurrentDateTime();
        survey.timestamp = getTimeStamp();
        survey.language = this.localeId;
        survey.startTime = stats.startTime;
        survey.endTime = stats.endTime;
        survey.duration = stats.duration;
        // Iterate through properties to get the agent name
        for (const prop in stats.agents) {
          if (stats.agents[prop].hasOwnProperty('name')) {
            survey.agent = stats.agents[prop].name;
            break;
          }
        }
      });
      oExtension.command('WebChatService.getSessionData').done((session): void => {
        survey.sessionId = session.sessionID;
        survey.alias = session.alias;
        survey.secureKey = session.secureKey;
        survey.userId = session.userId;
        resolve(survey);
      });
    });
  }
}
