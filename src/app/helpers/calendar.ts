export function getCurrentDay(): string {
    const date = new Date();
    const y = addZero(date.getFullYear());
    const m = addZero(date.getMonth() + 1);
    const d = addZero(date.getDate());
    return `${y}-${m}-${d}`;
}

export function getCurrentTime(): string {
    const d = new Date();
    const h = this.addZero(d.getHours());
    const m = this.addZero(d.getMinutes());
    const s = this.addZero(d.getSeconds());
    return `${h}:${m}:${s}`;
}

export function getTimeStamp(): number {
    const date = new Date();
    return date.getTime();
}
export function getCurrentDateTime(): string {
    const date = new Date();
    return date.toLocaleString('en-US');
}

function addZero(i) {
    if (i < 10) {
        i = '0' + i;
    }
    return i;
}