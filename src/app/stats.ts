export class Stats {
  ewt: number;
  calls: number;
  pos: number;
  aqt: number;
  wpos: number;
  time: number;
  clc: string;
  wt: number;
  wcalls: number
}
