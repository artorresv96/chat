import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Survey } from '../../survey';
import { Observable } from 'rxjs';
import { Banner } from 'src/app/banner';
import { Schedule } from 'src/app/schedule';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  public saveSurvey(survey: Survey) {
    return this.firestore.collection('surveys').add(Object.assign({}, survey));
  }

  public updateSurvey(docRef: string, survey: Survey) {
    return this.firestore.collection('surveys').doc(docRef).update({
      vote: survey.vote,
      comment: survey.comment
    });
  }

  public getBannerMessage(): Promise<Banner[]> {
    return new Promise(resolve => {
      this.firestore.collection('banner').snapshotChanges().subscribe(data => {
        resolve(data.map(elements => elements.payload.doc.data() as Banner))
      })
    })
  }

  public getSchedule(): Promise<Schedule> {
    return new Promise(resolve => {
      this.firestore.collection('schedule').snapshotChanges().subscribe(data => {
        resolve(Object.assign({}, ...data.map(elements => elements.payload.doc.data() as Schedule[])) as Schedule)
      })
    })
  }

  constructor(private firestore: AngularFirestore) { }
}
