import { TestBed } from '@angular/core/testing';

import { GenesysService } from './genesys.service';

describe('GenesysService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GenesysService = TestBed.get(GenesysService);
    expect(service).toBeTruthy();
  });
});
