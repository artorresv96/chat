import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Stats } from '../../stats';
import { catchError } from 'rxjs/operators'
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenesysService {

  statsUrl = this.localeId === 'es' ? environment.statServiceCl : this.localeId === 'pt' ? environment.statServiceBr : null;

  public getChatStats() {
    return this.http.get<Stats>(this.statsUrl)
      .pipe(
        catchError((err) => {
          console.log('error caught in service')
          return throwError(err);    //Rethrow it back to component
        })
      )
  }



  constructor(
    private http: HttpClient,
    @Inject(LOCALE_ID) protected localeId: string
  ) { }

}
