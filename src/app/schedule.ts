export class Schedule {
    startingDay: number;
    endingDay: number;
    startingHour: number;
    endingHour: number;
    blackoutDatesBrasil: string[];
    blackoutDatesSSC: string[];
}