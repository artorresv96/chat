export class Banner {
    language: string;
    message: string;
    isChatAvailable: boolean;
}