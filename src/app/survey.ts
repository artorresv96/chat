export class Survey {
  clientBP: string;
  startTime: string;
  endTime: string;
  duration: string;
  agent: string;
  client: string;
  sessionId: string;
  userId: string;
  alias: string;
  secureKey: string;
  datetime: string;
  timestamp: number;
  vote: number;
  comment: string;
  language: string;
  conversation: any[];
  agentMessages: number;
  systemMessages: number;
  userMessages: number;
  filesUploaded: number;
  waitingForAgent: number;
}
