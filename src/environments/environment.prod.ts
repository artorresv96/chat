export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyD_NeO2bo4jHBTwPS4tMQLtBSsAu1D--sg',
    authDomain: 'chatbot-latam.firebaseapp.com',
    databaseURL: 'https://chatbot-latam.firebaseio.com',
    projectId: 'chatbot-latam',
  },
  statServiceCl: 'https://ltm-gms.LTM.e-contact.cl/genesys/1/service/FS_CHAT_1',
  statServiceBr: 'https://ltm-gms.LTM.e-contact.cl/genesys/1/service/FS_CHAT_BR_1'
};
