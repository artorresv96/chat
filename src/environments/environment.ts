// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD_NeO2bo4jHBTwPS4tMQLtBSsAu1D--sg',
    authDomain: 'chatbot-latam.firebaseapp.com',
    databaseURL: 'https://chatbot-latam.firebaseio.com',
    projectId: 'chatbot-latam',
  },
  statServiceCl: 'https://ltm-gms.LTM.e-contact.cl/genesys/1/service/FS_CHAT_1',
  statServiceBr: 'https://ltm-gms.LTM.e-contact.cl/genesys/1/service/FS_CHAT_BR_1'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
