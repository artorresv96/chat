import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import * as jQuery from "jquery"

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

declare global {
  interface Window { _genesys: any; }
  interface Window { zESettings: any; }
//   interface Window {
//     jQuery: typeof jQuery;
//     $: typeof jQuery;
// }
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
